from django.shortcuts import render
from trips.models import Member
from django.contrib.auth.decorators import login_required

@login_required
def member_trips(request):
    member_info = Member.objects.filter(member=request.user)
    context = {
        "member_info": member_info,
    }
    return render(request, "trips/list.html", context)
