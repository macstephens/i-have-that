from django.db import models
from django.conf import settings


class Member(models.Model):
    name = models.CharField(max_length=100)
    bio = models.TextField()
    photo = models.URLField()
    member = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="members",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Trip(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    date = models.DateField(null=True)
    link = models.URLField(max_length=200)
    members = models.ManyToManyField(
        "Member",
        related_name="trips",
    )

    def __str__(self):
        return self.name


class Gear(models.Model):
    name = models.CharField(max_length=200)
    link = models.URLField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        "Member",
        related_name="gear",
        on_delete=models.CASCADE,
    )
    trip = models.ForeignKey(
        "Trip",
        related_name="gear",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
