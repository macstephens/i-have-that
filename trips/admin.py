from django.contrib import admin

from trips.models import Member, Trip, Gear


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "bio",
        "photo",
        "member",
    )


@admin.register(Trip)
class TripAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "date",
        "link",
        "get_members",
    )
    list_display_links = ["name"]

    def get_members(self, instance):
        for member in instance.members.all():
            return member.name


@admin.register(Gear)
class GearAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "link",
        "description",
        "owner",
        "trip",
    )
