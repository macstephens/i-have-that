from django.urls import path
from trips.views import member_trips

urlpatterns = [
    path("", member_trips, name="member_trips"),
    ]
